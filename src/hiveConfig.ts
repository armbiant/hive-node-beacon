import { ChainTypes, makeBitMaskFilter } from '@hiveio/hive-js/lib/auth/serializer';
import { QuotesService } from './quotes/quotes.service';

export const hiveNodes = [
  { name: 'api.hive.blog', endpoint: 'https://api.hive.blog' },
  { name: 'api.deathwing.me', endpoint: 'https://api.deathwing.me' },
  { name: 'hive-api.arcange.eu', endpoint: 'https://hive-api.arcange.eu' },
  { name: 'hived.emre.sh', endpoint: 'https://hived.emre.sh' },
  // { name: 'api.hivekings.com', endpoint: 'https://api.hivekings.com' },
  { name: 'api.openhive.network', endpoint: 'https://api.openhive.network' },
  { name: 'api.hive.blue', endpoint: 'https://api.hive.blue' },
  { name: 'rpc.ausbit.dev', endpoint: 'https://rpc.ausbit.dev' },
  { name: 'techcoderx.com', endpoint: 'https://techcoderx.com' },
  { name: 'anyx.io', endpoint: 'https://anyx.io' },
  { name: 'api.c0ff33a.uk', endpoint: 'https://api.c0ff33a.uk' },
  { name: 'hive.roelandp.nl', endpoint: 'https://hive.roelandp.nl' },
  { name: 'api.pharesim.me', endpoint: 'https://api.pharesim.me' },
  { name: 'hived.privex.io', endpoint: 'https://hived.privex.io' },
  { name: 'hive-api.3speak.tv', endpoint: 'https://hive-api.3speak.tv' },
  { name: 'hiveapi.actifit.io', endpoint: 'https://hiveapi.actifit.io' },
  { name: 'rpc.mahdiyari.info', endpoint: 'https://rpc.mahdiyari.info' },
  {
    name: 'rpc.ecency.com',
    endpoint: 'https://rpc.ecency.com',
    website_only: true,
  },
];

export const generateTests = (apiChainId, apiParamAccount, apiParamCommunity, beaconAccount, quotesService: QuotesService) => [
  {
    name: 'get_version',
    description: 'Get the current version of the node',
    type: 'fetch',
    method: 'database_api.get_version',
    params: {},
    score: 50,
    debug: false,
    validator: result => {
      return result.chain_id === apiChainId;
    },
  },
  {
    name: 'db_head_state',
    description: 'Get the current state of the node (less than 10 mins behind)',
    type: 'fetch',
    method: 'hive.db_head_state',
    params: {},
    score: 50,
    debug: false,
    validator: result => {
      return result.db_head_block > 50000000 && result.db_head_age < 600;
    },
  },
  {
    name: 'db_head_age',
    description: 'Check if the node is in sync with the chain (less than 30 secs behind)',
    type: 'fetch',
    method: 'hive.db_head_state',
    params: {},
    score: 15,
    debug: false,
    validator: result => {
      return result.db_head_age < 30;
    },
  },
  {
    name: 'dynamic_global_properties',
    description: 'Check chain global properties',
    type: 'fetch',
    method: 'database_api.get_dynamic_global_properties',
    params: {},
    score: 25,
    debug: false,
    validator: result => {
      return 'head_block_number' in result && 'hbd_interest_rate' in result;
    },
  },
  {
    name: 'feed_history',
    description: 'Get price feed history',
    type: 'fetch',
    method: 'database_api.get_feed_history',
    params: {},
    score: 15,
    debug: false,
    validator: () => true,
  },
  {
    name: 'get_accounts',
    description: 'Retrieve an account details',
    type: 'fetch',
    method: 'call',
    params: ['condenser_api', 'get_accounts', [[apiParamAccount]]],
    score: 25,
    debug: false,
    validator: result => {
      return Array.isArray(result) && result.length === 1;
    },
  },
  {
    name: 'get_relationship_between_accounts',
    description: 'Retrieve the relationship between two accounts',
    type: 'fetch',
    method: 'bridge.get_relationship_between_accounts',
    params: [beaconAccount, apiParamAccount],
    score: 15,
    debug: false,
    validator: result => {
      return result.follows === true && result.ignores === false;
    },
  },
  {
    name: 'get_post',
    description: 'Retrieve a single post and associated details',
    type: 'fetch',
    method: 'bridge.get_post',
    params: {
      author: 'hiveio',
      permlink: 'hive-first-community-hardfork-complete',
      observer: 'hiveio',
    },
    score: 15,
    debug: false,
    validator: result => {
      return result && result.post_id && result.children > 1;
    },
  },
  {
    name: 'get_ranked_by_created',
    description: 'Fetch recently created posts',
    type: 'fetch',
    method: 'bridge.get_ranked_posts',
    params: {
      tag: '',
      sort: 'created',
      limit: 20,
      observer: apiParamAccount,
    },
    score: 25,
    debug: false,
    validator: result => {
      const minTimestamp = new Date().getTime() - 1000 * 60 * 5;
      const minDate = new Date();
      minDate.setTime(minTimestamp);
      const fiveMinuteAgoString = minDate.toISOString().split('.')[0];

      return Array.isArray(result) && result.length === 20 && result[0].created > fiveMinuteAgoString;
    },
  },
  {
    name: 'get_ranked_by_trending',
    description: 'Fetch posts sorted by "trending"',
    type: 'fetch',
    method: 'bridge.get_ranked_posts',
    params: {
      tag: '',
      sort: 'trending',
      limit: 20,
      observer: apiParamAccount,
    },
    score: 15,
    debug: false,
    validator: () => true,
  },
  {
    name: 'get_account_posts_by_blog',
    description: 'Get posts in an user blog',
    type: 'fetch',
    method: 'bridge.get_account_posts',
    params: {
      account: apiParamAccount,
      sort: 'blog',
      limit: 20,
      observer: apiParamAccount,
    },
    score: 15,
    debug: false,
    validator: () => true,
  },
  {
    name: 'get_account_posts_by_feed',
    description: 'Get posts for an user "following" feed in correct order',
    type: 'fetch',
    method: 'bridge.get_account_posts',
    params: {
      account: apiParamAccount,
      sort: 'feed',
      limit: 10,
      observer: apiParamAccount,
    },
    score: 15,
    debug: false,
    validator: result => Array.isArray(result) && result.length > 0,
  },
  {
    name: 'get_account_posts_by_replies',
    description: 'Get replies to an user posts/comments',
    type: 'fetch',
    method: 'bridge.get_account_posts',
    params: {
      account: apiParamAccount,
      sort: 'replies',
      limit: 20,
      observer: apiParamAccount,
    },
    score: 15,
    debug: false,
    validator: () => true,
  },
  {
    name: 'get_community_pinned_and_muted',
    description: 'Get posts feed for a community and check for pinned/muted posts',
    type: 'fetch',
    method: 'bridge.get_ranked_posts',
    params: {
      tag: apiParamCommunity,
      sort: 'created',
      limit: 5,
      observer: apiParamAccount,
    },
    score: 15,
    debug: false,
    validator: result => {
      if (!Array.isArray(result) || result.length !== 5) {
        return false;
      }

      if (result[0].category !== apiParamCommunity) {
        return false;
      }

      if (!result[0].stats || !result[0].stats.is_pinned) {
        return false;
      }

      return true;
    },
  },
  {
    name: 'get_active_votes',
    description: 'Get votes on a specific post',
    type: 'fetch',
    method: 'condenser_api.get_active_votes',
    params: ['peakd', 'a-hive-launchpad-via-peak-open-projects-a-proposal'],
    score: 15,
    debug: false,
    validator: result => {
      return Array.isArray(result) && result.some(v => v.reputation > 0 || v.rshares > 0);
    },
  },
  {
    name: 'get_account_history',
    description: 'Load transaction history for an account (both activities and wallet transactions)',
    type: 'fetch',
    method: 'condenser_api.get_account_history',
    params: [beaconAccount, -1, 250, ...makeBitMaskFilter([ChainTypes.operations.transfer])],
    score: 15,
    features: ['get_account_history'],
    debug: false,
    validator: result => Array.isArray(result) && result.length > 50,
  },
  {
    name: 'get_transaction',
    description: 'Load a single transaction',
    type: 'fetch',
    method: 'condenser_api.get_transaction',
    params: ['c6b92bd4f2e052ff3718847f6f59e714bd5730d3'],
    score: 0,
    features: ['get_transaction'],
    debug: false,
    validator: result => {
      return result.transaction_id === 'c6b92bd4f2e052ff3718847f6f59e714bd5730d3';
    },
  },
  {
    name: 'get_rc_stats',
    description: 'Load stats about RC costs',
    type: 'fetch',
    method: 'rc_api.get_rc_stats',
    params: {},
    score: 10,
    features: ['get_rc_stats'],
    debug: false,
    validator: result => {
      return result.rc_stats && Object.keys(result.rc_stats.ops).length > 20;
    },
  },
  {
    name: 'custom_json',
    description: 'Try to cast a "custom_json" operation',
    type: 'cast',
    method: 'custom_json',
    params: {
      id: 'beacon_custom_json',
      json: JSON.stringify({ ping: 'pong', timestamp: new Date().toISOString() }),
      required_auths: [],
      required_posting_auths: [beaconAccount],
    },
    score: 10,
    features: ['broadcast'],
    debug: false,
    validator: () => true,
  },
  {
    name: 'transfer',
    description: 'Try to cast a "transfer" operation (validated as part of the chain consensus)',
    type: 'cast',
    method: 'transfer',
    params: {
      from: beaconAccount,
      to: beaconAccount,
      amount: '0.001 HIVE',
      memo: `${quotesService.getRandomQuote()} (${new Date().toISOString()})`,
    },
    score: 15,
    features: ['broadcast'],
    debug: false,
    validator: () => true,
  },
];
