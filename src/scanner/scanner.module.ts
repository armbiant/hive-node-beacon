import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { QuotesModule } from '../quotes/quotes.module';
import { HiveScannerService } from './hiveScanner.service';
import { HiveEngineScannerService } from './hiveEngineScanner.service';
import { HiveEngineHistoryScannerService } from './hiveEngineHistoryScanner.service';

@Module({
  imports: [QuotesModule],
  providers: [HiveScannerService, HiveEngineScannerService, HiveEngineHistoryScannerService, ConfigService],
  exports: [HiveScannerService, HiveEngineScannerService, HiveEngineHistoryScannerService],
})
export class ScannerModule {}
