import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron } from '@nestjs/schedule';
import * as SSC from '@hive-engine/sscjs';
import { hiveEngineNodes, generateTests } from '../hiveEngineConfig';

export type HiveEngineNodeTest = {
  name: string;
  description: string;
  type: string;
  method: string;
  endpoint: string;
  params: any;
  score: number;
  features?: string[];
  debug: boolean;
  validator: (result: any) => boolean;
};

export type HiveEngineNodeTestResult = {
  name: string;
  description: string;
  type: string;
  method: string;
  success: boolean;
  features?: string[];
};

export type HiveEngineNodeStatus = {
  name: string;
  endpoint: string;
  version?: string | null;
  updated_at: string;
  score: number;
  features?: string[];
  tests: HiveEngineNodeTestResult[];
};

const promiseWithTimeout = (promise: Promise<any>, timeoutMs: number) => {
  let timeoutHandle: NodeJS.Timeout;
  const timeoutPromise = new Promise((resolve, reject) => {
    timeoutHandle = setTimeout(() => reject(new Error(`Call timeout after ${timeoutMs}ms`)), timeoutMs);
  });

  return Promise.race([promise, timeoutPromise]).then(result => {
    clearTimeout(timeoutHandle);
    return result;
  });
};

@Injectable()
export class HiveEngineScannerService implements OnModuleInit {
  private readonly logger = new Logger(HiveEngineScannerService.name);

  private isRunning = false;

  private tests: HiveEngineNodeTest[] = [];

  private store: HiveEngineNodeStatus[] = [];

  constructor(private configService: ConfigService) {
    this.tests = generateTests();
  }

  onModuleInit() {
    this.scan();
  }

  getNodes(): HiveEngineNodeStatus[] {
    return this.store;
  }

  @Cron('5-59/10 * * * *')
  async scan(): Promise<boolean> {
    // skip if disabled
    if (this.configService.get<string>('ENABLE_HE', 'true').toLowerCase() !== 'true') {
      this.logger.warn('Scanner (HE) disabled -> skip');
      return;
    }

    // skip if already running
    if (this.isRunning) {
      this.logger.warn('Scanner already running -> skip');
      return;
    }

    this.isRunning = true;

    const apiCallTimeout = this.configService.get<number>('API_CALL_TIMEOUT') || 15000;

    try {
      const maxScore: number = this.tests.reduce((acc, cur) => {
        return acc + cur.score;
      }, 0);

      this.logger.log('Starting node scanner ...');

      const excludedNodes = this.configService.get<string>('EXCLUDED_NODES') ? this.configService.get<string>('EXCLUDED_NODES').split(',') : [];

      const nodes = hiveEngineNodes.filter(n => !excludedNodes.includes(n.name));

      this.logger.log('Configured nodes: ' + nodes.map(n => n.name));

      for (const node of nodes) {
        this.logger.log(`Switching node to: ${node.name}`);
        const ssc = new SSC(node.endpoint);

        let score: number = maxScore;
        const results: HiveEngineNodeTestResult[] = [];
        let nodeVersion = null;

        for (const test of this.tests) {
          try {
            if (test.type === 'fetch') {
              this.logger.log(`Call '${test.name}', params: ${JSON.stringify(test.params)}: ...`);
              const start = Date.now();

              const result = await promiseWithTimeout(ssc.send(test.endpoint, { method: test.method, params: test.params }), apiCallTimeout);

              if (test.method === 'getStatus') {
                nodeVersion = result.SSCnodeVersion;
              }

              if (test.debug) {
                this.logger.debug(`Call result: ${JSON.stringify(result)}`);
              }

              const success = test.validator ? test.validator(result) : true;
              const elapsed = Date.now() - start;

              if (success) {
                this.logger.log(`Call '${test.name}', completed in ${elapsed} ms`);
                results.push({
                  name: test.name,
                  description: test.description,
                  type: test.type,
                  method: test.method,
                  success: true,
                  ...(test.features && test.features.length ? { features: test.features } : {}),
                });
              } else {
                this.logger.warn(`Call '${test.name}', failed in ${elapsed} ms. Response: ${JSON.stringify(result)}`);
                score -= test.score;
                results.push({
                  name: test.name,
                  description: test.description,
                  type: test.type,
                  method: test.method,
                  success: false,
                });
              }
            }
          } catch (error) {
            this.logger.warn(`Call '${test.method}' failed for ${node.name}: ${error.toString()}`);
            score -= test.score;

            results.push({
              name: test.name,
              description: test.description,
              type: test.type,
              method: test.method,
              success: false,
            });
          }
        }

        const nodeScore = Math.round((score * 100) / maxScore);
        const nodeFeatures = [...new Set(results.flatMap(r => r.features || []))];

        // updated node status in local store
        const updatedNodeStatus = {
          name: node.name,
          endpoint: node.endpoint,
          version: nodeVersion,
          updated_at: new Date().toISOString(),
          score: nodeScore,
          features: nodeFeatures,
          tests: results,
        };

        const prevIndex = this.store.findIndex(stored => stored.name === updatedNodeStatus.name);
        if (prevIndex === -1) {
          this.store.push(updatedNodeStatus);
        } else {
          this.store.splice(prevIndex, 1, updatedNodeStatus);
        }
        this.logger.log(`Node scan completed for ${node.name}, score: ${nodeScore}`);
      }

      this.logger.log('Node scan completed successfully');
      return true;
    } catch (error) {
      this.logger.error(`Unexpected error during node scanning: ${error.toString()}`);
      return false;
    } finally {
      this.isRunning = false;
    }
  }
}
