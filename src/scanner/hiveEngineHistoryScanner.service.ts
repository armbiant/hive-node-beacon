import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron } from '@nestjs/schedule';
import * as engy from 'engy';
import { hiveEngineHistoryNodes, generateTests } from '../hiveEngineHistoyConfig';

export type HiveEngineHistoryNodeTest = {
  name: string;
  description: string;
  type: string;
  endpoint: string;
  params: any;
  score: number;
  features?: string[];
  debug: boolean;
  validator: (result: any) => boolean;
};

export type HiveEngineHistoryNodeTestResult = {
  name: string;
  description: string;
  type: string;
  success: boolean;
  features?: string[];
};

export type HiveEngineHistoryNodeStatus = {
  name: string;
  endpoint: string;
  version?: string | null;
  updated_at: string;
  score: number;
  features?: string[];
  tests: HiveEngineHistoryNodeTestResult[];
};

const promiseWithTimeout = (promise: Promise<any>, timeoutMs: number) => {
  let timeoutHandle: NodeJS.Timeout;
  const timeoutPromise = new Promise((resolve, reject) => {
    timeoutHandle = setTimeout(() => reject(new Error(`Call timeout after ${timeoutMs}ms`)), timeoutMs);
  });

  return Promise.race([promise, timeoutPromise]).then(result => {
    clearTimeout(timeoutHandle);
    return result;
  });
};

@Injectable()
export class HiveEngineHistoryScannerService implements OnModuleInit {
  private readonly logger = new Logger(HiveEngineHistoryScannerService.name);

  private isRunning = false;

  private tests: HiveEngineHistoryNodeTest[] = [];

  private store: HiveEngineHistoryNodeStatus[] = [];

  constructor(private configService: ConfigService) {
    this.tests = generateTests();
  }

  onModuleInit() {
    this.scan();
  }

  getNodes(): HiveEngineHistoryNodeStatus[] {
    return this.store;
  }

  @Cron('5-59/10 * * * *')
  async scan(): Promise<boolean> {
    // skip if disabled
    if (this.configService.get<string>('ENABLE_HE', 'true').toLowerCase() !== 'true') {
      this.logger.warn('Scanner (HE) disabled -> skip');
      return;
    }

    // skip if already running
    if (this.isRunning) {
      this.logger.warn('Scanner already running -> skip');
      return;
    }

    this.isRunning = true;

    const apiCallTimeout = this.configService.get<number>('API_CALL_TIMEOUT') || 15000;

    try {
      const maxScore: number = this.tests.reduce((acc, cur) => {
        return acc + cur.score;
      }, 0);

      this.logger.log('Starting node scanner ...');

      const excludedNodes = this.configService.get<string>('EXCLUDED_NODES') ? this.configService.get<string>('EXCLUDED_NODES').split(',') : [];

      const nodes = hiveEngineHistoryNodes.filter(n => !excludedNodes.includes(n.name));

      this.logger.log('Configured nodes: ' + nodes.map(n => n.name));

      for (const node of nodes) {
        this.logger.log(`Switching node to: ${node.name}`);
        engy.config.update('historyNode', node.endpoint);

        let score: number = maxScore;
        const results: HiveEngineHistoryNodeTestResult[] = [];
        let nodeVersion = null;

        for (const test of this.tests) {
          try {
            if (test.type === 'fetch') {
              this.logger.log(`Call '${test.name}', params: ${JSON.stringify(test.params)}: ...`);

              const start = Date.now();
              const result = await promiseWithTimeout(engy.history.get(test.endpoint, test.params), apiCallTimeout);
              if (test.debug) {
                this.logger.debug(`Call result: ${JSON.stringify(result)}`);
              }

              const success = test.validator ? test.validator(result) : true;
              const elapsed = Date.now() - start;

              if (success) {
                this.logger.log(`Call '${test.name}', completed in ${elapsed} ms`);
                results.push({
                  name: test.name,
                  description: test.description,
                  type: test.type,
                  success: true,
                  ...(test.features && test.features.length ? { features: test.features } : {}),
                });
              } else {
                this.logger.warn(`Call '${test.name}', failed in ${elapsed} ms. Response: ${JSON.stringify(result)}`);
                score -= test.score;
                results.push({
                  name: test.name,
                  description: test.description,
                  type: test.type,
                  success: false,
                });
              }
            }
          } catch (error) {
            this.logger.warn(`Call '${test.endpoint}', ${test.params} failed for ${node.name}: ${error.toString()}`);
            score -= test.score;

            results.push({
              name: test.name,
              description: test.description,
              type: test.type,
              success: false,
            });
          }
        }

        const nodeScore = Math.round((score * 100) / maxScore);
        const nodeFeatures = [...new Set(results.flatMap(r => r.features || []))];

        // updated node status in local store
        const updatedNodeStatus = {
          name: node.name,
          endpoint: node.endpoint,
          version: nodeVersion,
          updated_at: new Date().toISOString(),
          score: nodeScore,
          features: nodeFeatures,
          tests: results,
        };

        const prevIndex = this.store.findIndex(stored => stored.name === updatedNodeStatus.name);
        if (prevIndex === -1) {
          this.store.push(updatedNodeStatus);
        } else {
          this.store.splice(prevIndex, 1, updatedNodeStatus);
        }
        this.logger.log(`Node scan completed for ${node.name}, score: ${nodeScore}`);
      }

      this.logger.log('Node scan completed successfully');
      return true;
    } catch (error) {
      this.logger.error(`Unexpected error during node scanning: ${error.toString()}`);
      return false;
    } finally {
      this.isRunning = false;
    }
  }
}
